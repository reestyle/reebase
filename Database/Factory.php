<?php

namespace ReeBase\Database;

class Factory
{

	/*
	 * Constants
	 */
	const ADAPTER_MYSQL = 'MySQL';
	const ADAPTER_PHPIXIE = 'PHPixieORM';

	/**
	 * @var array
	 */
	static protected $_instances = [
		'named' => [],
		'numbered' => []
	];

	/**
	 * @var array
	 */
	static protected $_validAdapters = [
		self::ADAPTER_PHPIXIE,
		self::ADAPTER_MYSQL
	];

	/**
	 * @param $adapter
	 * @param null $instanceName
	 * @param null|\ReeBase\Config|array $config
	 *
	 * @return bool
	 * @throws \Exception
	 */
	static public function getAdapter($adapter, $instanceName = null, $config = null)
	{
		if (!in_array($adapter, static::$_validAdapters)) {
			throw new \Exception(sprintf('"%s" is not a valid Database adapter', $adapter));
		}

		$type = is_string($instanceName) ? 'named' : 'numbered';

		$ref = false;

		// When it's a numbered instance, iterate and return the first!
		if ($type == 'numbered') {
			foreach (static::$_instances[$type] as $index => $instance) {
				$adapterName = 'ReeBase\Database\Adapter\\' . $adapter;
				if ($instance instanceof $adapterName) {
					$ref = $index;
					break;
				}
			}
		} else {
			$ref = $instanceName;
		}

		if (!array_key_exists($ref, static::$_instances[$type])) {
			if ($adapter == self::ADAPTER_PHPIXIE) {
				static::$_instances[$type][$ref] = static::_setupPHPixieOrm();
			} else {
//				static::$_instances[$type][$ref] =
			}
		}

		return static::$_instances[$type][$ref];
	}

	/**
	 * Setup and return an instance of PHPPixieORM
	 *
	 * @param null|\ReeBase\Config|array $config
	 *
	 * @return null
	 */
	static protected function _setupPHPixieOrm($config = null)
	{
		$instance = null;

		if (null === $config) {
			$config = \ReeBase\Config::getInstance()->phpixie->dbs;
		}

		\ReeBase\Load::base('Database/Adapter/PHPixieORM');



		return $instance;
	}

}