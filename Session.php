<?php

namespace ReeBase;

Load::skeleton('Session');
use \ReeBase\Skeletons\SessionSkeleton as Skeleton;

/**
 * Class Session
 *
 * @package ReeBase
 */
class Session implements Skeleton
{

	const REGULAR_TYPE = 'regular';
	const COOKIE_TYPE = 'cookie';
	const DATABASE_TYPE = 'database';

	/**
	 * Session instance
	 * @var null|Session
	 */
	static protected $_instance = null;

	/**
	 * @var null|Skeleton
	 */
	protected $_sessionAdapter = null;

	/**
	 * Session token name for cookie
	 * @var string
	 */
	protected $_sessionCookieKey = 'reebase_session';

	/**
	 * Session type
	 * @var string
	 */
	protected $_sessionType = self::REGULAR_TYPE;

	/**
	 * Options for session type
	 * @var array
	 */
	protected $_sessionTypeOptions = array();

	/**
	 * Valid session types
	 * @var array
	 */
	protected $_validSessionTypes = array(
		self::REGULAR_TYPE, self::COOKIE_TYPE, self::DATABASE_TYPE
	);

	/**
	 * Get session instance
	 *
	 * @param string $type
	 *
	 * @return Session
	 */
	static public function getInstance($type = self::REGULAR_TYPE)
	{
		null === static::$_instance && (static::$_instance = new self($type));

		return static::$_instance;
	}

	/**
	 * Get session adapter
	 *
	 * @return null|Skeleton
	 */
	public function getSessionAdapter()
	{
		return $this->_sessionAdapter;
	}

	/**
	 * Initialize
	 *
	 * @param string $type
	 */
	public function __construct($type = self::REGULAR_TYPE)
	{
		$this->_sessionType = $type;
	}

	/**
	 * Start session
	 *
	 * @return Session
	 */
	public function start()
	{
		$this->initializeAdapter();

		return $this;
	}

	/**
	 * Start session
	 *
	 * @return Session
	 */
	public function destroy()
	{
		$this->initializeAdapter();

		$this->_sessionAdapter->destroy();

		return $this;
	}

	/**
	 * Set session setting
	 *
	 * @param string $var
	 * @param mixed $val
	 *
	 * @return Session
	 */
	public function set($var, $val)
	{
		$this->initializeAdapter();

		$this->_sessionAdapter->set($var, $val);

		return $this;
	}

	/**
	 * Get session setting
	 *
	 * @param string $var
	 * @param null|mixed $default
	 *
	 * @return Session
	 */
	public function get($var, $default = null)
	{
		$this->initializeAdapter();

		return $this->_sessionAdapter->get($var, $default);
	}

	public function forget($var)
	{
		$this->initializeAdapter();

		$this->_sessionAdapter->forget($var);

		return $this;
	}

	/**
	 * Initialize adapter
	 *
	 * @param bool $reinitialize
	 *
	 * @return Session
	 */
	public function initializeAdapter($reinitialize = false)
	{
		if (null === $this->_sessionAdapter || $reinitialize === true) {
			$adapter = ucfirst(strtolower($this->_sessionType));
			$adapterLoad = 'SessionAdapter/' . $adapter;
			$adapterClass = 'ReeBase\\SessionAdapter\\' . $adapter;

			Load::base($adapterLoad);

			$this->_sessionAdapter = new $adapterClass($this->_sessionTypeOptions);

			$this->_sessionAdapter->start();
		}

		return $this;
	}

	/**
	 * Set session type & options
	 *
	 * @param $type
	 * @param array $options
	 *
	 * @return Session
	 */
	public function setSessionType($type, array $options = array())
	{
		$this->_sessionType = in_array($type, $this->_validSessionTypes) ? $type : static::REGULAR_TYPE;

		$this->_sessionTypeOptions = $options;

		return $this;
	}

	/**
	 * Magic setter for session values
	 *
	 * @param $var
	 * @param $val
	 */
	public function __set($var, $val)
	{
		$this->_sessionAdapter->set($var, $val);
	}

	/**
	 * Magic setter for session values
	 *
	 * @param $var
	 *
	 * @return mixed
	 */
	public function __get($var)
	{
		return $this->_sessionAdapter->get($var, null);
	}

}