<?php

namespace ReeBase\Http;

use ReeBase\Server;

class Request
{

	/**
	 * Is it a POST request
	 *
	 * @return bool
	 */
	public function isPost()
	{
		return $this->isRequestMethod('post');
	}

	/**
	 * Is it a GET request
	 *
	 * @return bool
	 */
	public function isGet()
	{
		return $this->isRequestMethod('get');
	}

	/**
	 * Get request method
	 *
	 * @return bool|string
	 */
	public function getRequestMethod()
	{
		return Server::getRequestMethod();
	}

	/**
	 * Test for type of request method if it's outside POST or GET (PUT, LIST, custom, etc.)
	 *
	 * @param string $method
	 *
	 * @return bool
	 */
	public function isRequestMethod($method)
	{
		return strcasecmp($this->getRequestMethod(), $method) === 0;
	}

	/**
	 * Filter POST entries
	 *
	 * @param array $postKeys
	 *
	 * @return array
	 */
	public function filterPost(array $postKeys)
	{
		$postKeys = array_fill_keys($postKeys, false);

		if ($this->isPost()) {
			foreach ($postKeys as $postKey => $value) {
				$postKeys[$postKey] = array_key_exists($postKey, $_POST) ? $_POST[$postKey] : false;
			}
		}

		return $postKeys;
	}

}