<?php

namespace ReeBase\SessionAdapter;

use ReeBase;

ReeBase\Load::skeleton('Session');

use ReeBase\Skeletons\SessionSkeleton as Skeleton;

/**
 * Class Cookie
 *
 * @package ReeBase\SessionAdapter
 */
class Cookie implements Skeleton
{

	/**
	 * Options
	 * @var array
	 */
	protected $_options = array();

	/**
	 * Cookie data
	 * @var array
	 */
	protected $_cookieData = array();

	/**
	 * Destroy session
	 * @var bool
	 */
	protected $_destroy = false;

	/**
	 * Initialize
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array())
	{
		$this->_options = $options;
	}

	/**
	 * Start session
	 *
	 * @return Cookie
	 */
	public function start()
	{
		session_set_cookie_params($this->_options['lifetime'], '/', ReeBase\Server::getHostname());

		$this->_cookieData = $this->decryptData(
			$_COOKIE[$this->_options['cookiename']]
		);

		return $this;
	}

	/**
	 * Stop session
	 *
	 * @return Cookie
	 */
	public function destroy()
	{
		$this->_destroy = true;

		return $this;
	}

	/**
	 * Set a setting
	 *
	 * @param $var
	 * @param $val
	 *
	 * @return Cookie
	 */
	public function set($var, $val)
	{
		$this->_cookieData[$var] = $val;

		return $this;
	}

	/**
	 * Remove a setting
	 *
	 * @param $var
	 *
	 * @return Cookie
	 */
	public function remove($var)
	{
		unset($this->_cookieData[$var]);

		return $this;
	}

	/**
	 * Get a setting
	 *
	 * @param $var
	 * @param null $default
	 *
	 * @return mixed|null
	 */
	public function get($var, $default = null)
	{
		return array_key_exists($var, $this->_cookieData) ? $this->_cookieData[$var] : $default;
	}

	/**
	 * Save data on object destroy
	 */
	public function __destroy()
	{
		if ($this->_destroy) {
			setcookie($this->_options['cookiename'], null, time() - 3600);
		} else {
			$cookieData = $this->encryptData();

			setcookie(
				$this->_options['cookiename'],
				serialize($cookieData),
				$this->_options['lifetime']
			);
		}
	}

	/**
	 * Encrypt data if necessary
	 *
	 * @return array
	 */
	public function encryptData()
	{
		$cookieData = $this->_cookieData;
		if (
			array_key_exists('encrypt', $this->_options) && $this->_options['encrypt']
			&& array_key_exists('cryptkey', $this->_options)
		) {
			$cookieData = $this->encryptRecursive($cookieData);
		}

		return $cookieData;
	}

	/**
	 * Perform recursive encryption on arrays
	 *
	 * @param array $array
	 *
	 * @return array
	 */
	public  function encryptRecursive(array $array)
	{
		$encrypted = array();
		foreach ($array as $index => $data) {
			if (is_array($data)) {
				$data = $this->encryptRecursive($data);
			}

			mcrypt_encrypt(MCRYPT_BLOWFISH, $this->_options['cryptkey'], $data, MCRYPT_MODE_ECB);

			$encrypted[$index] = $data;
		}

		return $encrypted;
	}

	/**
	 * Decrypt data
	 *
	 * @return array
	 */
	public function decryptData()
	{
		$cookieData = $this->_cookieData;
		if (
			array_key_exists('encrypt', $this->_options) && $this->_options['encrypt']
			&& array_key_exists('cryptkey', $this->_options)
		) {
			$cookieData = $this->encryptRecursive($cookieData);
		}

		return $cookieData;
	}

	/**
	 * Decrypt data recursively
	 *
	 * @param array $array
	 *
	 * @return array
	 */
	public function decryptRecursive(array $array)
	{
		$decrypted = array();
		foreach ($array as $index => $data) {
			if (is_array($data)) {
				$data = $this->decryptRecursive($data);
			}

			mcrypt_decrypt(MCRYPT_BLOWFISH, $this->_options['cryptkey'], $data, MCRYPT_MODE_ECB);

			$decrypted[$index] = $data;
		}

		return $decrypted;
	}

}