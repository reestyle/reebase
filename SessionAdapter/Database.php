<?php

namespace ReeBase\SessionAdapter;

use ReeBase;

ReeBase\Load::skeleton('Session');

use ReeBase\Skeletons\SessionSkeleton as Skeleton;

/**
 * Class Database
 *
 * @package ReeBase\SessionAdapter
 */
class Database implements Skeleton
{

	/**
	 * Start session
	 *
	 * @return $this
	 */
	public function start()
	{
		throw new \Exception('Session database adapater not implemented');

		return $this;
	}

	/**
	 * Stop session
	 *
	 * @return $this|mixed
	 */
	public function destroy()
	{
		throw new \Exception('Session database adapater not implemented');

		return $this;
	}

	/**
	 * Set a setting
	 *
	 * @param $var
	 * @param $val
	 *
	 * @return $this
	 */
	public function set($var, $val)
	{
		$_SESSION[$var] = $val;

		return $this;
	}

	/**
	 * Get a setting
	 *
	 * @param $var
	 * @param null $default
	 *
	 * @return mixed|null
	 */
	public function get($var, $default = null)
	{
		return array_key_exists($var, $_SESSION) ? $_SESSION[$var] : $default;
	}

}