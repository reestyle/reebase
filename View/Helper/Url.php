<?php

namespace ReeBase\View\Helper;

use ReeBase;

\ReeBase\Load::skeleton('ViewHelper');

use ReeBase\Skeletons\ViewHelperSkeleton as Skeleton;

/**
 * Class Url
 *
 * @package ReeBase\View\Helper
 */
class Url implements Skeleton
{

	/**
	 * @var null|\ReeBase\View
	 */
	protected $_view = null;

	/**
	 * Initialize
	 *
	 * @param ReeBase\View $view
	 */
	public function __construct(ReeBase\View $view)
	{
		$this->_view = $view;
	}

	/**
	 * Invoke
	 *
	 * @param mixed $params
	 *
	 * @return string
	 */
	public function invoke($params)
	{
		if (array_key_exists(0, $params)) {
			$actionParts = $params[0];

			if (is_array($actionParts)) {
				$actionPath = array_reverse(array_slice($actionParts, 0, 3));

				$params = array();
				if (count($actionParts) > 3) {
					$params = array_slice($actionParts, 3);
				}

				$url = implode('/', $actionPath);
			} else {
				$url = trim($actionParts, ' /');
			}
		}

		$addPathInfo = true;
		if (array_key_exists(1, $params)) {
			$addPathInfo = $params[1] === true;
		}

		$dispatcher = ReeBase\Registry::getInstance('dispatcher');

		$baseUri = $dispatcher->getBaseUri();

		return ($addPathInfo ? $baseUri : '') . '/' . $url;
	}

}