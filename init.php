<?php

// Load & boot up configuration
require_once APP_BASE . '/base/Load.php';
ReeBase\Load::base(
	array(
		'Config',
		'ErrorHandler',
		'Controller',
		'Hooks',
		'Dispatcher',
		'Layout',
		'View',
		'Server',
		'Client',
		'Session',
		'Registry'
	)
);

//ReeBase\Config::getInstance();
ReeBase\ErrorHandler::setup();
