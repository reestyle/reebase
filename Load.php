<?php

namespace ReeBase;

/**
 * Class Load
 *
 * @package ReeBase
 *
 * This class is tightly coupled to the base
 */
class Load
{

	/**
	 * Add an include path
	 *
	 * @param $path
	 */
	public static function addToIncludePath($path)
	{
		if (strpos(get_include_path(), $path) === false) {
			set_include_path(get_include_path() . ':' . $path);
		}
	}

	/**
	 * Load skeleton
	 *
	 * @param String $skeleton name of skeleton, do not append 'Skeleton'
	 */
	public static function skeleton($skeleton)
	{
		static::components('base/Skeletons', $skeleton . 'Skeleton');
	}

	/**
	 * Load something from the system
	 *
	 * @param String $name
	 */
	public static function base($name)
	{
		static::components('/base', $name);
	}

	/**
	 * Load a vendor
	 *
	 * @param $vendor
	 * @param $relativePath
	 */
	public static function vendor($vendor, $relativePath)
	{
		static::components('vendors/' . $vendor, $relativePath);
	}

	/**
	 * Load a controller
	 *
	 * @param String $module
	 * @param String $controller
	 *
	 * @return string
	 * @throws \Exception
	 */
	public static function controller($module, $controller)
	{
		$module = strtolower($module);

		/*
		 * Check if the given module exists - if not, set to default
		 *
		 * @todo This is strange behaviour - it should raise an error or throw one
		 */
		if (is_dir(APP_BASE . '/modules/' . $module)) {
			$folder = '/modules/' . $module;
		} else {
			$folder = '/modules/default';
		}

		/*
		 * Add suffix to folder
		 */
		$folder .= '/controllers';

		/*
		 * Check if module folder exists
		 */
		static::checkApplicationDir($folder, 'Module "%s" does not exist', array($module));

		/*
		 * Add suffix to controller file name
		 */
		$controller .= Config::getInstance()->global->controllers->suffix;

		/*
		 * Try and load it
		 */
		try {
			return static::components($folder, $controller);
		} catch (\Exception $exception) {
			\ReeBase\ErrorHandler::handleException($exception);

			throw new \Exception(
				sprintf('Could not load controller "%s" in module "%s"', $controller, $module),
				500
			);
		}
	}

	/**
	 * Check if file exists within application folder
	 *
	 * @param String $folder
	 * @param String $message
	 * @param array $messageParams
	 * @param bool $throw If the method should throw an exception or return a boolean on unsuccessfull attempt
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function checkApplicationDir($folder, $message, array $messageParams, $throw = true)
	{
		if (!is_dir(APP_BASE . $folder)) {
			if ($throw) {
				throw new \Exception(
					vsprintf($message, $messageParams),
					500
				);
			} else {
				return false;
			}
		}

		return true;
	}

	/**
	 * Load components form a specific directory
	 *
	 * @param String $folder Where to look
	 * @param String $files Which files to load
	 *
	 * @return string
	 * @throws \Exception
	 */
	public static function components($folder, $files)
	{
		$files = !is_array($files) ? array($files) : $files;

		$baseLocation = APP_BASE . '/' . trim($folder, ' /');
		foreach ($files as $file) {
			$fileLocation = $baseLocation . '/' . $file . '.php';

			if (!is_file($fileLocation)) {
				throw new \Exception(
					vsprintf('Could not locate file "%s" in "%s"', array($file, $folder))
				);
			}

			require_once $fileLocation;
		}

		return $baseLocation;
	}

}