<?php

namespace ReeBase;

class Server
{

	/**
	 * Get request method
	 *
	 * @return bool|string
	 */
	static public function getRequestMethod()
	{
		return static::getValue('REQUEST_METHOD', false);
	}

	/**
	 * Get server IP address
	 *
	 * @return bool|string IPv4
	 */
	static public function getIPAddress()
	{
		return static::getValue('SERVER_ADDR', false);
	}

	/**
	 * Get hostname
	 *
	 * @return bool|string
	 */
	static public function getHostname()
	{
		return static::getValue('SERVER_NAME', false);
	}

	/**
	 * Get port number
	 *
	 * @return bool|integer
	 */
	static public function getPort()
	{
		return static::getValue('SERVER_PORT', false);
	}

	/**
	 * Get protocol
	 *
	 * @return bool|string
	 */
	static public function getProtocol()
	{
		return static::getValue('SERVER_PROTOCOL', false);
	}

	/**
	 * Check if the superglobal $_SERVER has a key
	 *
	 * @param string $key
	 *
	 * @return bool
	 */
	static public function hasKey($key)
	{
		return array_key_exists($key, $_SERVER);
	}

	/**
	 * Check if the superglobal $_SERVER has a key
	 *
	 * @param string $key
	 * @param false|mixed $default
	 *
	 * @return bool
	 */
	static public function getValue($key, $default = false)
	{
		return static::hasKey($key) ? $_SERVER[$key] : $default;
	}

	/**
	 * Is HTTP Secure protocol used
	 *
	 * @param integer $port
	 *
	 * @return bool
	 */
	static public function isHttps($port = null)
	{
		return
			(strcasecmp(static::getValue('HTTPS', 'off'), 'ON') === 0)
			&& ($port !== null ? static::getPort() == $port : true);
	}

	/**
	 * Test for AJAX request
	 *
	 * @return bool
	 */
	static public function isAjaxRequest()
	{
		return strcasecmp(static::getValue('HTTP_X_REQUESTED_WITH', false), 'xmlhttprequest') === 0;
	}

}