<?php

namespace ReeBase\Skeletons;

use ReeBase;

interface ViewHelperSkeleton
{

	public function __construct(ReeBase\View $view);

	public function invoke($params);

}