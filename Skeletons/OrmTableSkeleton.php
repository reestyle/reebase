<?php

interface OrmTableSkeleton
{

	static public function tableExists($table);

	public function readTable();

	static public function makeTable($meta);

	public function updateTable($meta);

}