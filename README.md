= README

This is a new try at a (H)MVC framework (H stands for hierarchial, means using modules as hierarchy), where you can implement your own ORM/DBAL. It has a View, Controller, Registry and Config object with what you can do most of the configuration. Just about everything is working, though probably not yet loaded with functionality. There is, however, a possibility to implement your own View class, as long as it implements the ViewSkeleton.

== Public
It is not yet ready for large scaled enterprise solutions (no LDAP or other authentication layers are included -yet-, nor is SOAP - though you can surely make your own interface).

== Goals
It must not be bulky and everything should be balanced between speed and ease of use. Beginners and moderate (OO) developers should be able to unserstand it as well as advanced developers. 

== Base functionality
It is working with a 'PATH_INFO' approach: index.php/module/controller/action/param1/param2...

== Need example?
If you need some basic code on how to get started, just leave a message...
