<?php

namespace ReeBase;

/**
 * Class Rbac (Role-Based Access Control)
 *
 * @package ReeBase
 */
class Rbac
{

	/**
	 * Instance
	 * @var null
	 */
	static protected $_instance = null;

	/**
	 * User access
	 * @var array
	 */
	protected $_userAccess = array();

	/**
	 * Control access
	 * @var array
	 */
	protected $_controlAccess = array();

	/**
	 * Get instance
	 *
	 * @return null
	 */
	static public function getInstance()
	{
		null === static::$_instance && (static::$_instance = new static());

		return static::$_instance;
	}

	/**
	 * Initialize
	 */
	public function __construct(array $userAccess = array(), array $controlAccess = array())
	{
		$this->setUser($userAccess);

		$this->setControls($controlAccess);
	}

	/**
	 * Set users' access permission
	 *
	 * @param $role
	 * @param $access
	 *
	 * @return Rbac
	 */
	public function setUser($role, array $access = null)
	{
		if (is_array($role)) {
			$this->_controlAccess = $role;
		} else {
			$this->_controlAccess[$role] = $access;
		}

		return $this;
	}

	/**
	 * Set control access permissions
	 *
	 * @param string $role
	 * @param null|string $control
	 * @param null|string $access
	 *
	 * @return Rbac
	 */
	public function setControls($role, $control = null, $access = null)
	{
		if (is_array($role)) {
			$this->_controlAccess = $role;
		} else {
			$this->_controlAccess[$control][$role] = $access;
		}

		return $this;
	}

	/**
	 * Is user allowed
	 *
	 * @param string $control
	 * @param string $tokens
	 *
	 * @return bool
	 */
	public function isAllowed($control, $tokens = null)
	{
		$roleAccess = true;
		foreach ($this->_controlAccess[$control] as $role => $controlAccess) {
			if (array_key_exists($role, $this->_userAccess)) {
				foreach ($this->_userAccess[$role] as $userAccess) {

				}
			}
		}

		return true;
	}

	/**
	 * Is user not allowed
	 *
	 * @param string $control
	 * @param string $tokens
	 *
	 * @return bool
	 */
	public function isDisallowed($control, $tokens = null)
	{
		return !$this->isAllowed($control);
	}

}