<?php

namespace ReeBase;

class Registry
{

	/**
	 * Internal data
	 * @var array
	 */
	static protected $_data = array();

	/**
	 * Get an instance of something you need - only use this if you really want the instance, but not if you
	 * want to test for the instance
	 *
	 * @param null $name
	 * @param $className
	 * @param array $options
	 *
	 * @return mixed
	 */
	static public function getInstance($name, $className = null, $options = array())
	{
		if (!array_key_exists($name, static::$_data)) {
			$className = null !== $className ? $className : $name;

			if (strpos($className, '\ReeBase') === 0) {
				$load = str_replace('\\', '/', substr($className, 9));

				Load::base($load);
			} else {

			}

			static::$_data[$name] = new $className($options);
		}

		return static::$_data[$name];
	}

	/**
	 * Set an instance - overrides any previous version
	 *
	 * @param $name
	 * @param $object
	 *
	 * @return mixed
	 */
	static public function setInstance($name, $object)
	{
		static::$_data[$name] = $object;

		return static::$_data[$name];
	}

	/**
	 * Check if an instance exists
	 *
	 * @param $name
	 *
	 * @return bool
	 */
	static public function hasInstance($name)
	{
		return array_key_exists($name, static::$_data);
	}

}