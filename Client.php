<?php

namespace ReeBase;

class Client
{

	/**
	 * Get server IP address
	 *
	 * @return bool|string IPv4
	 */
	static public function getIPAddress()
	{
		return static::hasKey('REMOTE_ADDR') ? $_SERVER['REMOTE_ADDR'] : false;
	}

	/**
	 * Get hostname
	 *
	 * @return bool|string
	 */
	static public function getHostname()
	{
		return static::hasKey('REMOTE_HOST') ? $_SERVER['REMOTE_HOST'] : false;
	}

	/**
	 * Get port number
	 *
	 * @return bool|integer
	 */
	static public function getPort()
	{
		return static::hasKey('REMOTE_PORT') ? $_SERVER['REMOTE_PORT'] : false;
	}

	/**
	 * Check if the superglobal $_SERVER has a key
	 *
	 * @param $key
	 *
	 * @return bool
	 */
	static public function hasKey($key)
	{
		return array_key_exists($key, $_SERVER);
	}

}