<?php

namespace ReeBase;

Load::skeleton('View');
use ReeBase\Skeletons\ViewHelperSkeleton;
use ReeBase\Skeletons\ViewSkeleton as Skeleton;

/**
 * Class View
 *
 * @package ReeBase
 */
class View implements Skeleton
{

	/**
	 * View data
	 * @var array
	 */
	protected $_data = array();

	/**
	 * View data
	 * @var array
	 */
	protected $_helpers = array();

	/**
	 * View location
	 * @var null|string
	 */
	protected $_viewLocation = null;

	/**
	 * Script name
	 * @var null|string
	 */
	protected $_scriptName = null;

	/**
	 * Magic getter
	 *
	 * @param $var
	 *
	 * @return mixed
	 */
	public function __get($var)
	{
		return array_key_exists($var, $this->_data) ? $this->_data[$var] : null;
	}

	/**
	 * Easy setter
	 *
	 * @param $var
	 * @param $val
	 *
	 * @return mixed
	 */
	public function __set($var, $val)
	{
		$this->_data[$var] = $val;
	}

	/**
	 * Generic setter
	 *
	 * @param string|array $var
	 * @param mixed $val [Optional]
	 *
	 * @return View
	 */
	public function set($var, $val = null)
	{
		if (is_array($var)) {
			$this->_data = array_merge($this->_data, $var);
		} else {
			$this->$var = $val;
		}

		return $this;
	}

	/**
	 * Set view location
	 *
	 * @param $viewLocation
	 *
	 * @return View
	 * @throws \Exception
	 */
	public function setViewLocation($viewLocation)
	{
		if (!is_dir($viewLocation)) {
			throw new \Exception(vsprintf('View location (%s) not found', array($viewLocation)));
		}

		$this->_viewLocation = $viewLocation;

		return $this;
	}

	/**
	 * Set script name
	 *
	 * @param null|string $scriptName
	 *
	 * @return View
	 */
	public function setScriptName($scriptName)
	{
		if (null === $scriptName) {
			$scriptName = Dispatcher::getInstance()->getAction();
		}

		$this->_scriptName = $scriptName;

		return $this;
	}

	/**
	 * Render view
	 *
	 * @param null|string $template
	 * @param bool $returnOnly
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function render($template = null, $returnOnly = false)
	{
		if (null === $this->_viewLocation) {
			throw new \Exception('View location not set');
		}

		if (null !== $template) {
			$this->_scriptName = $template;
		}

		$viewPath = $this->_viewLocation . '/' . $this->_scriptName . '.phtml';

		ob_start();
		require $viewPath;
		$content = ob_get_clean();

		if (!$returnOnly) {
			print $content;
		}

		return $content;
	}

	/**
	 * Set options by array
	 *
	 * @param array $options
	 *
	 * @return Layout
	 */
	public function setOptions(array $options)
	{
		foreach ($options as $optionName => $setting) {
			$method = 'set' . $optionName;
			if (method_exists($this, $method)) {
				call_user_func(array($this, $method), $setting);
			}
		}

		return $this;
	}

	/**
	 * Invoke a helper
	 *
	 * @param $helper
	 * @param $params
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function __call($helper, $params)
	{
		$helper = strtolower($helper);

		if (!array_key_exists($helper, $this->_helpers)) {

			$helperFilename = ucfirst($helper);

			$filePath = dirname(__FILE__) . '/View/Helper/' . $helperFilename . '.php';

			if (is_file($filePath)) {
				require_once $filePath;
			} else {
				throw new \Exception(sprintf('Unable to load helper "%s"', $helperFilename));
			}

			$className = '\ReeBase\View\Helper\\' . ucfirst($helper);

			$this->registerHelper($helper, new $className($this));
		}

		return $this->_helpers[$helper]->invoke($params);
	}

	/**
	 * Register a viewhelper
	 *
	 * @param $name
	 * @param ViewHelperSkeleton $object
	 *
	 * @return $this
	 */
	public function registerHelper($name, ViewHelperSkeleton $object)
	{
		$this->_helpers[$name] = $object;

		return $this;
	}

}